<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        DB::table('rules')->insert(
            [
                ['title' => "Visitor"],
                ['title' => "Owner"],
                ['title' => "Admin"]
            ]
        );
    }
}
