<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->string('deal');
            $table->string('pays');
            $table->string('ville');
            $table->string('adress');
            $table->string('owner');
            $table->integer('livingroom');
            $table->integer('bedroom');
            $table->integer('kitchen');
            $table->integer('toilet');
            $table->text('otherdetail');
            $table->string('mainview');
            $table->string('otherview');
            $table->boolean('state');
            $table->unsignedBigInteger('user_id')->index();
            $table->timestamps();

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('restrict')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
